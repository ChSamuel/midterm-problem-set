# Problem Set 3
## Refactoring with Design Patterns

In this package, you are faced with a code base of certain RPG Game simulation.
The basic RPG Game includes the variation of **Character** and **Weapon**. 
There are several **Weapon** already implemented in this codebase:

1. Bow
2. Broadsword
3. Crossbow
4. Knife
5. Mace
6. Pike
7. Shield
8. Spear
9. Wizard Staff

Each variation of Character have their own speciality:

1. Archer
    * Can only wield one **Bow**.
2. Assasin
    * Can hold one or two **Knife**
    * Can only wield one **Broadsword** 
3. Barbarian
    * Can hold one or two **Broadsword**
    * Can only wield one **Pike**
    * Can only wield one **Spear** 
4. Cleric
    * Can only hold one **Mace**
    * Can only hold one **Shield**
5. Knight
    * Can hold one or two **Broadsword**
    * Can only hold one **Shield**
6. Mage
    * Can only hold one **Wizard Staff**

To begin your journey of debugging this code, you may run the `GameSimulationClient` class. Below is the sample output of the program.
Input represented with prefix `>>>>`
```



Choose your desired class (type the class name):
1. Archer 2. Assasin 3. Barbarian
4. Cleric 5. Knight 6. Mage

>>>> assasin

Type the name of your Character...

>>>> Munaf

Your Character successfully spawned. Here is the bio...
Assasin Lv.1-Munaf
Successfully equip Ebony Knife
Successfully equip Ivory Knife
Next step:
1. Create and Equip new Left Hand Weapon
2. Create and Equip new Right Hand Weapon
3. Drop Left Hand Weapon
4. Drop Right Hand Weapon
5. Finish Customization

>>>> 5

Your Character successfully spawned. Here is the bio...
Assasin Lv.1-Munaf
Enjoy your game!!!
```

There are several problems with this codebase:

1. There are default implementations for `strike, chantSpell, chantHealingPoem, block` implemented at `Weapon` class.
It is inherited across all weapons but at the same time several methods being overriden.
2. If there are any new **Weapon** or **Character** implemented, the `GameSimulationClient` class must be changed. Moreover,
the `GameSimulationClient` also responsible to implement the complexity of spawning **Weapon** or **Character**.

and many more problems still exist other than the previous two.

### How to use

```java
javac *.java
java GameSimulationClient
```

### Questions

1. Finish the implementation of `GameSimulationClient` class at Line of Code 97-186.
**HINT:** method `setRightWeapon, setLeftWeapon, dropLeftWeapon, dropRightWeapon` return a string representing
the response message of performing certain action. Print that message to console.
you successfully use the method, you must print it to console
2. Refactor your codebase after finishing the previous question to solve ***Two Problems*** mentioned in the description. 
Use your knowledge of **Design Patterns, Design Principles, and Code Smells**. **Make sure that each refactoring step 
is illustrated using one git commit**.
3. After finishing the previous questions, perform Refactoring for **any other problems** in this codebase. 
Use your knowledge of **Design Patterns, Design Principles, and Code Smells**. **Make sure that each refactoring step 
is illustrated using one git commit**. Don't forget to mention the problems you found below.
```markdown
Replace this text with the problems you found in this code base when solving question number 2 and 3.
```